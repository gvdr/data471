---
title: "Contract"
date: 2019-01-31T15:16:47+13:00
draft: FALSE
---

## Overview

DATA471 will stimulate students to think about the ethical facets of their data scientific projects and provide them with conceptual and practical tools to assess the projects. The ethics and security of data collection, storage, manipulation, analysis and communication is of paramount importance in our information based society. This course faces these topics from the point of view of data scientists—rather than consumers or data subjects—enabling the student to become trustworthy professionals. The students will learn to identify risk and opportunities related to fairness, agency, interpretability, and security. Māori Data Sovereigneity, Te Mana Raraunga, and its relevance for data scientist in New Zealand will be introduced. The course will follow a flipped class-room flow. Fundamental concepts will be first introduce via guided discussions and hands-on-data exercises during the laboratories. In the lectures, the understanding of concepts and tools introduced in the laboratories is made rigorous and generalised. 

The course is run thanks to a number of important collaborations: [**Te Mana Raraunga**](https://www.temanararaunga.maori.nz/) will provide essential insight on Maori Data Sovereignty; [**Data 4 Democracy**](https://www.datafordemocracy.org/project/global-data-ethics-project) is providing a much needed ethical framework for data scientists.

The course will provide a safe and supportive environment in which students can express their ideas, explore their ethical frameworks and collaborate to find common ground. Students will need to be familiar with basic data science concepts and techniques.


## Learning outcomes 

At the end of this course, students will be able to:  

-   Identify ethical risks (biases, privacy violations, …)  in data science projects.  
-   Prevent, mitigate, or remediate unethical data science projects.  
-   Perform rudimentary data anonymization techniques and explain their importance.  
-   Understand other stakeholders’ ethical requests.  
-   Identify when Te Mana Raraunga is relevant in a data science project.
 
## Assessments

The course will be assessed by active partecipation, participation in assignments posted on LEARN, and a final group project as follows:

### Reflection 40%
Each student will need to **write a glossary entry** elucidating the meaning of some key concept (a list of concepts will be built during the course) and to **peer review** the contribution of two other students (20%).

Each student will be asked to choose one of the course weeks (a lab and its lecture) and produce an **introductory discussion of the week's topic**. The discussion should be readable by somebody that has not taken the course. You can decide wether you want to make it a writing, video, or podcast piece (20%). It's your call to pick either a more mathematical or a more humanistic take.

### Labs 30%.
The course is organised in three blocks (ethical problems, ethical requests, ethical commitment). Each block has one hands-on-data laboratory (you will work in pairs) and three class discussion sessions. By the end of each block, students are asked to submit their **hands-on-data exercises** (these are marked) and the pre- and post- discussion **reflection question** (I won't mark these, but just let me see that you did work on them). 

### Group Project 30%
In the final group project students will be asked to develop a **communication project** (it can be a podcast, a blog post, a dashboard, a website, a video,...) **describing a data science product** (that you either built of that you analysed). The students will have to show that they considered the ethical facets of the data science product,  applying the conceptual and technical tools we tackled in class.  
The emphasis is on the depth and originality of ethical analysis, not on the extension of the data science product.
Teams will be assembled in the first weeks of the course, and will have the full time of the course to work on the deliverable: please keep me in the loop, so I can help you throughout the development of the project.

## Course contact hours

There is a two hour lab and a one hour lecture each week (lab before lecture).

-   **Labs**  
    Mondays 09:00--11:00 Jack Erskine 244 (discussion sessions) and Jack Erskine 442 (labs)
-   **Lectures**  
    Tuesday 10:00--11:00 Jack Erskine 445

Please note these can be subject to change and it is recommended that you check these times in the first couple of weeks of term.

#### office hours:
Tuesdays 11:00--12:00 (after the lectures). If it does not suit you, please contact me and we'll arrange otherwise. 
 
## Contacts
Lecturer, Course Coordinator  
Giulio Valentino Dalla Riva  
Erskine Building, 710    
giulio.dallariva@canterbury.ac.nz
