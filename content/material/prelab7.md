---
title: "Prelab8: who owns the data?"
date: 2019-04-25T18:26:06+12:00
draft: false
---

On Monday we are going to start a discussion that may take a bit to survey.

The big question is, dangerously, easy to pose:

# Who owns the data?

Yet, it's not an easy question to answer, because many different it involves different point of views and epistemological dimensions (that is, "ways of knowing the wold").

To start with, let's listen this podcast episode by Walter Vannini:

<a class="spreaker-player" href="https://www.spreaker.com/user/runtime/dk-1x03eng" data-resource="episode_id=12972282" data-theme="dark" data-autoplay="false" data-playlist="false" data-cover="https://d3wo5wojvuv7l.cloudfront.net/images.spreaker.com/original/bf63af1378c58a189f9f17b7b0e40201.jpg" data-width="100%" data-height="400px">Listen to "DK_en 1x03 - The Mother Of All Datagrabs" on Spreaker.</a><script async src="https://widget.spreaker.com/widgets.js"></script>

And let's read this interview with Prof. Tahu Kukutai https://www.radionz.co.nz/news/te-manu-korihi/343647/data-sovereignty-new-global-guidelines-for-indigenous-health


#### If you have time

Start reading "Te Mana Raraunga - Māori Data Sovereignty Network Charter": http://planetmaori.com/Files/Content/2016/Te_Mana_Raraunga_Charter.pdf

Also this bit on how the European law on personal data protection (GDPR) affects data science can be interesting: https://thomaswdinsmore.com/2017/07/17/how-gdpr-affects-data-science/
