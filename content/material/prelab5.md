---
title: "Prelab 5: agency"
date: 2019-03-14T14:33:42+13:00
draft: false
---

Week 5 opens the segment about "_requests_": what do data subjects (be they individual, communities, interest group, or any other _informational organism_ in our _infosphere_) want?


<img src="/img/noun_autonomy.png" width="270" >

# Agency

[**Agency**](https://plato.stanford.edu/entries/agency/) and [**autonomy**](https://plato.stanford.edu/entries/personal-autonomy) are (complex) notions that speak about the right of an _agent_ (an _informational organism_ in the _infosphere_) to determine their own direction, to take decision freely, to self-govern. When thinking about these, we may want to keep in mind that informational organisms do not exist in isolation, but are part of large, convoluted, networks of interactions: what does it mean to have autonomy in, and as, a community? How can we give agency to informational organisms in a tangled network of mutual dependencies and relations?

<iframe src="https://giphy.com/embed/2ibdqd1waBVg4" width="370" height="480" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>

We will talk about it starting from a very problematic, and delicate, episode. On November 25th, 2018 the Rohingya refugees in Bangladesh went into strike for three days to protest against the way that UNHRC (the UN organization for refugees) was collecting their biometric data. They demanded an halt to the forced "smart card" identification process, as they were worried that the data collection would have facilitated their discrimination. You can read their [**requests in full here**](https://pbs.twimg.com/media/Ds5w-DOVAAAr5Sf.jpg). [**Reuters covered the story**](https://in.reuters.com/article/myanmar-rohingya-bangladesh-idINKCN1NV1DD).

One year before this protest, [**Zara Rahman**](https://twitter.com/@zararah) wrote about the perils of collecting sensible biometric information from a persecuted people: [**Irresponsible data? The risks of registering the Rohingya**](https://www.irinnews.org/opinion/2017/10/23/irresponsible-data-risks-registering-rohingya)

## Activities

1.  Reading the piece from Zara Rahman, try to understand if and how self-determination and autonomy were at stake here, and motivate your answer.
2.  Can you find other examples in the (recent or not) where data and data analysis were used to either support, enable, limit, or deprive somebody (be they individuals or communities) of their agency / autonomy?

## Further resources

A follow up on IRIN news about [**refugee activism**](https://www.irinnews.org/news-feature/2018/11/27/bangladesh-rohingya-strike-highlights-growing-refugee-activism).

Jaron Lannier on the need to [**remake the internet**](https://www.ted.com/talks/jaron_lanier_how_we_need_to_remake_the_internet) (here to be read as, the industries relying on internet) to allow for more agency.

A conversation about [**Rethinking The Internet: How We Lost Control And How To Take It Back**](https://youtu.be/HJDCZDAPgS8) (notice, it's a longish video and it touches a lot of different points).
