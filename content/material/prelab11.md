---
title: "Prelab11"
date: 2019-05-17T13:36:40+12:00
draft: false
---

# Data Science as carpentry

According to philosopher Ian Bogost, ***carpentry*** is the activity of "making things that explain how things make their world." You may want to re-read the notion a number of times, as beyond its simplicity there is quite a lot to unpack.

Just a couple of hints. ***Making their world*** can be put in relation to the notion of _ontological commitment_ that we discussed last week: the way in which interact the world _makes_ the world, for us. The reference to ***things*** (or objects) can be put in relation to that idea we discussed in the first week of _flat ontologies_, that is recognizing that other things, indeed all the things, have the same _reality_ and we can not discount them or ignore them; things are not neutral.

Ian Bogost and Katherine Behar (see the first chapter of Object Oriented Feminism: [**An Introduction to OOF**](https://s3.amazonaws.com/arena-attachments/938007/aedd021dd58dec2513d9f007f6d727c9.pdf)), use the concept of _carpentry_ to talk about philosophy.

Yet, I believe the notion of "making things that explain how things make their world" is a perfect description of Data Science. The things we make (algorithms, dashboards, report, models, ...) are there to explain (describe, clarify, predict, ...) how other things (objects, agents in the infosphere, people, tools, other algorithms, ...) make their world, often interacting. And as we do it, we are making our worlds as well.

## Carpentry: truth and truthfulness

One thing we can learn from carpentry, is the difference between true and truthful. Alberto Cairo, a skilled data visualizer, talks about [**"truth continuum"**](https://journalismcourses.org/courses/DES17/Chapter3TheTruthfulArt.pdf).

### Activity

Read the chapter by Alberto Cairo and think: can we adapt what he talks about to data science in general, and not just to data visualization?
