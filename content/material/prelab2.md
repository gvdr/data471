---
title: "Prelab2"
date: 2019-02-21T15:16:01+13:00
draft: false
---

# Digital poverty cages

In the next discussion session we are going to talk about ***poverty cages***: what are they? how are they built? what is the impact of their digitalization? what role do data science product play in their development?

## readings

**mandatory**:  
[**A Child Abuse Prediction Model Fails Poor Families**](https://www.wired.com/story/excerpt-from-automating-inequality/): by Virginia Eubanks

**if you want more**:  
[High-Tech Homelessness](https://www.americanscientist.org/article/high-tech-homelessness): by Virginia Eubanks.

### reflection question

Sketch down your answer before the lab, and think about it after. For this week, do not worry about submitting it (we are going to do it starting from the 5th lab).

-    What is the ethical issue in this scenario?  
-    What was the responsibilities of the data scientists leading the project discussed in the main article?  
-    Who should have been involved in the design process?

## further readings

[**Virginia Eubanks**](https://virginia-eubanks.com)' "Automating Inequality" presents a thorough analysis and history of digital poverty cages, with examples that are extremely relevant for New Zealand. See it in the [**reading list**](/settings). Also, follow her on [**twitter**](https://twitter.com/PopTechWorks). And, hey, she's coming to Auckland on the [**15th of March**](https://are-we-automating-inequality-in-aotearoa-an-afternoon-with-prof.lilregie.com/booking/attendees/new)!

[**Algorithms Designed to Fight Poverty Can Actually Make It Worse**](https://www.scientificamerican.com/article/algorithms-designed-to-fight-poverty-can-actually-make-it-worse/): by Virginia Eubanks. (it's hard to get access to it, so write me if you want to read it.)

[**Fighting poverty with data**](http://science.sciencemag.org/content/353/6301/753.full) by Joshua Evan Blumenstock. (you do have access through the UC library portal)
