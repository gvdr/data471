---
title: "Week 4, prelab activities"
date: 2019-03-07T10:54:57+13:00
draft: false
---

# Critical!

No reading this week, but a good watching. We will be talking about [**recommender systems**](https://en.wikipedia.org/wiki/Recommender_system), homophily, [**assortativity**](https://en.wikipedia.org/wiki/Assortativity), weapons of math destruction, and more, starting from this 40 minutes (plus intro and question time) talk by Prof. Wendy H. K. Chun:

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Qhp80UXTvaQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## activity

1.  Prof. Chun identifies 4 key steps to overcome discriminating uses of data science. Summarise them with your own words. (< 4 lines)
2.  What is a recommender system? (< 4 lines)
3.  Prof. Chun talks in length about "homophily":
    -    What is homophily? Answer both pointing at the sociological meaning and at its algorithmic / data scientific meaning. (< 4 lines + some maths)
    -    What issues (both scientific and ethical) poses the assumption of homophily in recommender systems? (< 4 lines + some maths)

## additional material

(**pick one of the following**)

1.   Take a look at the work of [**Guillaume Chaslot**](https://algotransparency.org/), and the story as told in the MIT Technology Review: [**here**](https://www.technologyreview.com/s/610760/an-ex-google-engineer-is-scraping-youtube-to-pop-our-filter-bubbles)  
2.   Prof. [**Zeynep Tufkeci**](https://twitter.com/zeynep) wrote a great piece for The New York Times about a recommender system and [**radicalization**](https://www.nytimes.com/2018/03/10/opinion/sunday/youtube-politics-radical.html)  
3.   A great book is the one written by [Prof. Safiya U. Noble](https://safiyaunoble.com/), "Algorithms of oppression" (the intro is freely available [**here**](https://nyupress.org/webchapters/Noble_%20AlgorithmsofOppression_intro.pdf) and the book is available in my office, just come get it!)  
4.   Some data science: Prof. Walter Quattrociocchi and colleagues is performing extremely interesting research about the role [**polarization**](http://wadam.dis.uniroma1.it/pubs/misinformation.pdf) and [**echo chambers**](https://www.nature.com/articles/srep37825) in social networks.  
