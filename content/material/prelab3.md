---
title: "Pre laboratory 3 activities"
date: 2019-02-27T14:46:03+13:00
draft: false
---

# Lab 3: bias


![](https://dam-prod.media.mit.edu/thumb/2018/02/05/gs10.png.1400x1400.png) 
<p><tiny>Credit: Joy Buolamwini</tiny> </p>



## Logistics

### Room:

This Monday 4<sup>th</sup> of March, the laboratory is going to be in  
  

-   **Jack Erskine 442**

and not in the usual Jack Erskin 244, as we are going to work on computers.

### Stencila

The data science hands-on-data lab will be run on [**Stencila Hub**](https://hub.stenci.la/beta/?next=/me/signup/). Stencila Hub is an online (and desktop) suite for reproducible research.

All activities will be completely hosted online, so you don't need to install anything if you don't want to (but a recent web browser).

You will only need to sign-up for a free account on stencila, and everything else will follow from there.

However, [**Stencila**](https://stenci.la/) is an open source project under active development. That means: do expect glitch and occasional bugs. We are working with bleeding edge technology, and that requires some patience.

Keep an eye on your email, as I will send you a token for signing up.

## Pre-lab activities

We are going to work on bias in data science. Or, that is symmetrics, to consider fairness and inclusivity in data science.

Before coming to the lab, take a look at some of [**Joy Buolamwini**](https://www.poetofcode.com) work on bias in Face Recognition software.

Thanks to the talent of Joy Buolamwini we can set the background and be inspired by some poetry at the same time (3:32):

<div style="max-width:854px"><div style="position:relative;height:315;"><iframe width="560" height="315" src="https://www.youtube.com/embed/QxuyfWoVV98" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
<p>
  you can find a transcript of Boulamwini's poem in the description of her video <a href="https://www.youtube.com/watch?v=QxuyfWoVV98">here</a>.
</p>
</div>


And dive deeper watching her [**talk at TEDxBeaconStreet**](https://www.ted.com/talks/joy_buolamwini_how_i_m_fighting_bias_in_algorithms) (8:44):

<div style="max-width:854px"><div style="position:relative;height:315;"><iframe width="560" height="315" src="https://www.youtube.com/embed/UG_X_7g63rY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
<p>
  you can find a transcript of Boulamwini's talk <a href="https://www.ted.com/talks/joy_buolamwini_how_i_m_fighting_bias_in_algorithms/transcript">here</a>.
</p>
</div>

Joy Boulamwini's and [**Timnit Gebru**](ai.stanford.edu/~tgebru/)'s research on "Intersectional Accuracy Disparities in Commercial Gender Classification", or more shortly "Gender Shades", is a very interesting paper to read (highly suggested!). You can find it [**here**](http://proceedings.mlr.press/v81/buolamwini18a/buolamwini18a.pdf). (website of the project [**here**](http://gendershades.org/))

## reflection questions

Before the lab, try to answer these questions:
 
-  What is "bias" for a data scientific product?  
-  What are the causes of bias in the facial recognition software?
-  What is "fairness"?  
-  How can we identify the biases of a data scientific product?

(notice: we are not collecting answers yet, this will start from week 4, as most student will have settled on which courses to attend at that point.)

## Further reading

A book that we will talk about quite a bit is [Cathy O'Neil](https://mathbabe.org)'s ["Weapons of Math Destruction"](https://weaponsofmathdestructionbook.com/). You find it in the library, and I have a copy in my office for you to take if you want.
