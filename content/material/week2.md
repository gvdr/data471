---
title: "Week 2, Lab and Lecture"
date: 2019-02-26T14:27:38+13:00
draft: false
---

Week 2

**Warning: I'm running late writing these notes; this is only a stub, thought for you to keep track of what we had done. I hope to complete them very soon. And, remember, you can help me: the source material is at www.gitlab.com/gvdr/data471/content/material**

# The Digital Poor House. Or, Is Data Neutral?

## Lab 2: The actor-network account of the Allegheny Family Screening Tool debacle

### The poor house

In the pre-lab activities for this week, I inveted you to read Virginia Eubanks article for [**Wired**](https://www.wired.com/story/excerpt-from-automating-inequality/): it is a long excerpt from her book ["Automating Inequality"]. And it is really worth for you to read it before we go on to anything else, so go read it. But don't worry, I'll still be here when you'll be done.

It's a long read, so take your time to go through it.

...

...

...

...

...

...

...

Done? Not yet? Back to the article, this notes will not disappear so be patient and read.

...

...

...

...

...

...

...

That will do, back to business here.

Poverty --- a widespread, urgent, problem --- is not new. Different societies, in different times in history, have historically faced economical inequality in different ways. Some of these approaches aim at enabling and supporting people in poverty to overcome that status. Other approaches aim at keeping the not-poors away from the bothering presence of poors, that is, at reducing the negative impact of poverty on part of the society by isolating poverty. Most modern policies oscillate between these two poles.

A _poverty cage_ is a set of _actors_ ("things" that act, that do something: institutions, policies, places, people, ...) which actions (more or less consciously coordinated) make it more difficult for people in poverty to change their condition, exiting poverty or mitigating its negative consequences. Poverty cages are key instruments to tackle poverty in the "isolation" approach just mentioned. Virginia Eubanks books can be read as an history of the _poor house_ in the United States. A poor house, in its pre-digital implementation, was a house were people in poverty were forced to live if they wanted to receive any support from the state. In their management, they were punitive toward people in poverty (the idea was that, if they were too comfortable, than people would just _choose_ to live there). And they did very little, or better nothing, to reduce the number of people in poverty or improve their conditions.

Far from having disappeared, Eubanks claims, they are now even more pervasive in their _digital_ implementation. People in poverty are caged within invisible walls built by data science products and algorithms that concur in keeping them in poverty, for example denying them loans, access to welfare, or filtering people in poverty away from jobs and other opportunities.

The phenomenon existed before digitalization (an account of the poor house in London in 1902 is offered by [**Jack London**](http://london.sonoma.edu/Writings/PeopleOfTheAbyss/)), but digitalization drammatically changed the _scale_, the volume and the pervasivity, of the poor house.

### Is that modernization?

Building on Eubanks article, we exercised our ethical imagination analysing what happened when the republican government of Indiana, USA, decided to "_modernize_" the Allegheny Family Screening Tool<sup>[**1**](#week2_1)</sup>, has the role of identifying kids with high risk of being abused.

You should have read the article above, so I feel free to summarise it wildly: the traditional system relied on social workers: it was heavily "humans in the loop"; the humans could have a bias, but were also slow in applying it and flexible in correcting it; the modernization of the screening tool limited the role of the humans in the loop.

But did it remove the bias? Did it make the system more just? Or did it automitize the bias? And, is the automatic system able to capture the complexity of each family?

And, what was, if any, the responsibility of each _actor_ in this process?

#### Actor, network

To answer, we decided to play one of our "games", instead of trying to hit straight for an answer.

We first tried to sketch a concise account of the events, sticking to what we thought were the "facts" (the temptation of jumping straight to conclusions about "good" and "bad" decisions was strong, but we fought against it). Then, reading three different accounts of the story, we isolated all the possible players, or ***actors*** (either human, group of humans, or non-humans) having a role in the story. We filled a white board with it. To be surer that we considered most of them, we used the _"yes, and"_ game.

Then, we thought about how these actors _interacted_ and, all together, formed a _network_. Using these _actor-networks_ we told the stories once again, paying attention at who too-simple accounts can be misleading.

It turns out that this other game, the "actor-network game"<sup>[**2**](#week2_2)</sup>, is a simplified take on a much more serious, and ponderous, sociological theory proposed firstly by Bruno Latour in his classic "We have Never Been Modern". One of the central tenets of the vision of the world championed by Latour is that of substantial symmetry between human and non-human "players". In the next lecture we will see that is indeed fruitful to adopt a "flat ontology", giving the same opportunity to all things, to all stuff (there comprehending people), of playing an active role.


### Where is the data scientist?

## Lecture 2

Back in Italy I used to ride an old pre-loved Vespa: it took me few instants to understand that that bike had a mind of her own. She would start (or not) based on a mood. Even distances, between my home-town and the town I went to school at, did depend on her mood. Sometimes is was a smooth 15minutes commute. Other times it was a perillous, hard, 45 minute endurance competition. After a while, I started to believe that the reality itself of that street depended, at least in part, on what the Vespa wanted to do. In any case, it was a very different road from the same one I used to travel by bus.

Clearly, that is just me being silly, right? Objects, Vespas, do not "want" things, and they do not really change reality. Don't they?

### Are data and/or tools neutral?

#### What neutral means?

#### Masonry, glasses, algorithms

### What is data?

#### Raw data does not exist

#### Data ↔ Capta

# Notes

<a name="week2_1">1</a>: In the class I erronously referred to the "Indiana Family and Social Services Administration" instead of the Allegheny institution. Sorry about that. The Indiana's case is another one that Virginia Eubanks writes about in her book.
<a name="week2_2">2</a>: If you are interested, you can start to read more about it here:  
Latour, Bruno. _Reassembling the social: An introduction to actor-network-theory._ Oxford university press, 2005.
