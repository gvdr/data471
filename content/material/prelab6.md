---
title: "Prelab6, Algorithmic Fairness"
date: 2019-03-22T11:14:53+13:00
draft: false
---


# TAF is tough, but is worth

This week we are going to talk about fairness, and in particular the ethical request of being treated fairly _algorithmically_.

We are going to consider a little handful of materials, or better a subset of them.

Read the points about fairness here: http://www.fatml.org/resources/principles-for-accountable-algorithms

Done? Let's move to a completely different kind of source. Recently, Alexandra Ocasio-Cortez spoke about algorithms. It's been widely reported by media, and discussed in depth (you can search it on the web for many commentaries).

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr"><a href="https://twitter.com/AOC?ref_src=twsrc%5Etfw">@AOC</a> Algorithms are made by human beings (based on human assumptions) <br><br>If you don&#39;t fix the bias then you automate the bias... <a href="https://twitter.com/hashtag/AI?src=hash&amp;ref_src=twsrc%5Etfw">#AI</a> <a href="https://twitter.com/hashtag/DigitalEthics?src=hash&amp;ref_src=twsrc%5Etfw">#DigitalEthics</a> <a href="https://t.co/4VhEXh05eb">pic.twitter.com/4VhEXh05eb</a></p>&mdash; Cristobal Cobo (@cristobalcobo) <a href="https://twitter.com/cristobalcobo/status/1088046833086148608?ref_src=twsrc%5Etfw">January 23, 2019</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

## Activities

-  What is fairness? Write down a concise answer. If it can help you, you can use some mathematical notation to help your ideas.
-  Can algorithms be fair, or unfair? Do not jump straight to a conclusion: try and write down some arguments for both sides, and think about what we have mentioned regarding "neutrality".


### submissions

I've opened "diary" for you on learn. Please use that to submit your homework.
