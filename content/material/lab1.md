---
title: "Laboratory 1"
date: 2019-02-19T15:52:11+13:00
draft: false
---

# DATA471 Laboratory 1 (Monday 18 2019)

## How do we talk about ethics?

### engaging respectfully

As for any discipline, talking about ethical problems and opportunities in a classroom requires agreeing on some rule.

The main rule that I, as an educator, impose (it is not negotiable) is that you, the students, commit to **engage respectfully**. You will have to interact with me and with your peers, and you will have to do in a constructive way that foster a healthy, inclusive, fair community. Our course is not a one man show, and avoiding contacts and interactions is not allowed. If you are shy, scared, or in any other way not comfortable in interacting with the class, please let me know so that I can do whatever it is possible for us to create an environment where it is actually possible for you to engage.

<iframe src="https://giphy.com/embed/pPd9XthNGcARhencgK" width="480" height="480" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>

(in class we discussed about our Code of Conduct, policies for the assignment, and all the rest of our [**educational contract**](/contract))

### arguing

In the course, both in the laboratories and in the lectures, we will **discuss** a lot. I strongly invite you to express your doubts, and contradict me if you think I am in the wrong. That may well be! Yet, we need to distinguish between discussing as in "fighting" and discussing as in "dialoguing". We aim for the second. We believe that we can have meaningful, fruitful, dialogues about ethics, if everybody tries to.

Walter Sinnot-Armstrong offers a compelling _4 step_ guide to have good conversation in his ["Think Again: How to Reason and Argue"](https://www.penguin.co.uk/books/293942/think-again/9780141983110.html) book (you should be able to find it in the University library, or in the city central library, or ask me a copy). In class, we listend to his short piece for [**Aeon**](https://aeon.co/ideas/reach-out-listen-be-patient-good-arguments-can-stop-extremism). It's a strongly suggested short reading.

<iframe src="https://giphy.com/embed/Tw5xHBhuiG5gY" width="480" height="376" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>

W.S.A. summarises the steps in:

1.   **reach out**: we need to make an active effort to understand each others. We need to this sistematically, carefully, and charitably. We start from the point of view that other people have something genuinally interesting to say, and it is also upon us to understand their reasons. Reaching out _carefully_ means that we pay attention to other people arguments, and do not confuse our prejudice of what they want to express with what they actually are saying. We reach out _charitably_ when our default position is to consider the other a smart, savvy person: we can all fall into little logic slips, say the wrong word, use the wrong pronounciation (I know that very well!), or more in general expose a strong idea not in the strongest shape. Let's try to identify the strong idea, and work with that, rather than running after the little glitches: let's call out "logical fallacies" only when they really matter.

2.  **ask questions**: it takes a village to raise ethical awareness, we can't do it alone. Asking questions is an active practice, it requires training, and is an important part of increasing our understanding. Therefore, we should questions implicit assumptions, scrutinize implications, and ask for clarification whenever we need to.

3.  **be patient**: discussion may get heated, or too personal, or too technical, or too fast. Ethical thinking is slow thinking. Slow down, choose you words, wait for others to end their sentences, don't rush to conclusions. It's ok not to reach a final word on a subject, and it is valuable the understanding we gather in the process.

4.  **give arguments**: Ethical arguments matter. **Why** we think something is important. Do not try to impose your opinion, but share your background, make us part of your thinking. In our course we are going to use a number of specialized vocabularies (those of mathematics, philosophy, computer science, statistics, ...). We want to talk about things using a language that is as clear as we can make it. We need to make this effort in order to help others in understing our arguments.

If you are interested, W.S.A. runs a (free) [**online course**](https://www.coursera.org/learn/understanding-arguments) on this very topic.

## The why game, part 1.

The first case study that we are going to encounter is about the infamous _Cambridge Analytica_ scandal. Wikipedia has a quite comprehensive page about [**what happened**](https://en.wikipedia.org/wiki/Cambridge_Analytica).

The _why_ game is one of the techniques we are going to use repeatedly in the course. It has a quite simple mechanism: try to express clearly and succintly your position about something, and then ask yourself _why_ you think that's the case.

In this case we can think that CA behaviour has been overall ethical or unethical. My personal understanding is that they violated the ethical request of Facebook users and betrayed the trust of many citizens across different countries. You may disagree, and that's ok, we can talk about it. Yet, the important part here is asking _why_ you think that's the case. And keep asking it until you reach to a point where you have a hard time going deeper.

For example, I may think that CA behaviour was wrong _because_ they collected information not only about users that gave a consent, but also about their friends, who may not have given any consent. And I believe that this is wrong _because_ I believe that user should be able to consent to their profiling. And I believe that this is _because_ we value our free agency. And I believe that we value human agengy _because_ that is what makes us people, and limiting it would violate our humanity. And, now, I would have a hard time explaining _why_ I think that. So I stop it here, and look at the process that went from my basic assumptions (free agency is an inviolable part of humanity)  to the specific conclusion (or conclusions). Is it a consistent path? Do I stand by each of the steps? Are they well related to the facts? Are there gaps? Sometimes everything is smooth, and we stand by our conclusions. Sometimes we find issues, and we need to fix them.

This is just an example, and maybe not the most compelling argument for the ethicity (or lack of) of CA.

> **exercise 1**: read up a bit about Cambridge Analytica and try to state your position. Do it in short, atomic, phrases (phrases that say only one thing at a time) and play the _why_ game. What do you discover about your basic assumptions? Did you change your mind about your conclusions?  
>  
> This is a process that we can do alone, to prepare for a dialogue and clear our ideas, or as a community, to find (if possible) a common ground about an ethical issue.

In the next lecture we are going to use the _why_ game to explore more about the CA scandal and the recent OpenAI decision that their [**text generation model was too dangerous**](https://blog.openai.com/better-language-models/) to open to the public.

## Class breakdown

-  Short introduction, round of names, know each other, Code of Conduct, educational contract. (10')  
-  Why do you attend the course? What question would you like the course to tackle? We sketched ethical questions (on post its), read and rewrote them (again, on post its), and organised them by topic. (40')
-  Aerobic break. (5')
-  We listened to W.S.A. piece for Aeon (link above) and we discussed what it means to discuss ethics in a fruitful way (10' + 20').
-  We played the _why_ game talking about Cambridge Analytica. (15')
