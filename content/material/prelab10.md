---
title: "Prelab 10 activities"
date: 2019-05-10T13:52:50+12:00
draft: false
---

# Oaths, principles, checklists

A large variety of workers, in fields as diverse as psychology, engineerings, justice, medicine, journalism, ... have developed _oaths_ stating their commitment to a set of _ethical principles_.

Also data scientists from all over the world are elaborating, discussing, and adopting such oaths.

During the weekend, take a look at some examples:

-  The Data 4 Democracy initiatives (a large, international, grass-root gathering of data scientists across industry and Academy) elaborated an ethical [**framework**](https://www.datafordemocracy.org/documents/GDEP-Ethics-Framework-Principles-one-sheet.pdf) presented as a [**pledge**](https://www.datafordemocracy.org/pledge) (see also the [**manifesto**](https://datapractices.org/manifesto/)).
-  In the US, the "Committee on Envisioning the Data Science Discipline" published a rewrite of the _Hippocratic Oath_ (which was originally taken by physicians in ancient Greece) updated for the data science practitioners. It's published by the US' National Academy of Sciences, and you can find it [**here**](https://www.nap.edu/read/25104/chapter/13).

Cathy O'Neill (we have read her already, when talking about Weapons of Math Destruction), wrote an interesting piece about [**the role of ethical oaths**](https://www.wired.co.uk/article/data-ai-ethics-hippocratic-oath-cathy-o-neil-weapons-of-math-destruction) in data science (caveat: it's a long interview)

## activities

-  Skim the oaths: are they compatible? What makes them different?
-  Search online for other, similar, ethical principles, guidelines, or checklists for data science: both the Statistical Association and the ACM (the computer programmers association) recently developed or updated their code of ethics. Can you find any other one?
-  Reflect: would you commit to an ethical guideline? If so, why? And if not, why not?
