---
title: "Settings"
date: 2019-01-31T15:15:27+13:00
draft: FALSE
---

## Outline

The course is organised in three blocks (ethical issues, ethical requests, ethical commitment, loosely following Simon Critchley's [**Infinitely Demanding**](https://www.versobooks.com/books/1135-infinitely-demanding) structure of disappointment, request, and commitment). Each block contains one hands-on-data laboratory, three guided discussion sessions, and four frontal lectures.

The hands-on-data laboratory are based on R via [Stenci.la](https://stenci.la/) (but students can choose to use any other programming language they and the instructor are familiar with). Each guided discussion requires the students to perform some preliminary reading. Frontal lectures follow laboratories and add rigour, deep and context to students understanding.

# Lectures plan

| Block | Laboratory | Lecture |
|-------|------------|---------|
| Issues 1 | Discussion: Cambridge Analytica [**notes**](/material/lab1) | [**Information Ecology**](/material/lect1) |
| Issues 2 | Discussion: Digital Poverty Cages [pre-lab](/material/prelab2) [**notes**](/material/week2) | Is Raw Data Neutral Data? [**notes**](/material/week2) |
| Issues 3 |  Hands-on-data: Bias by Proxy [**pre-lab**](/material/prelab3) | Black Box Data Science| 
| Issues 4 | Discussion: Recommender Systems [**prelab**](/material/prelab4) | Weapons of Math Destruction |
| Requests 1 | Discussion: Agency [**prelab**](/material/prelab5) | Agency |
| Requests 2 | Discussion: Fairness Accountability and Transparency [**prelab**](/material/prelab6) | Fair treatment |
| Requests 3 |  Hands-on-data: Anonymyzation | Security |
| Requests 4 |  Discussion: Who owns Data? [**prelab**](/material/prelab7) | Trust |
| Commitment 1 | Hands-on-data: Level of Abstraction & Point of View | Diversity & Inclusiveness |
| Commitment 2 | Discussion: Oaths, Codes, Guidelines [**prelab**](/material/prelab10) | Responsibility |
| Commitment 3 | Discussion: (Data) Carpentry [**prelab**](/material/prelab11) | Openness |
| Commitment 4 | Discussion: Data Sovereigneity | Individuals & Communities |

# Suggested material

For the course you are requested to do your best to read, watch, listen wide and wild from the material I will mention in class. There's no single compulsory book, but an evergrowing list of suggested materials to pick from.

Consult at list one of this suggestions. If no one of the mentioned material gets your interest, ask me.

I'll try to mention here all the books I talk about in class, but probably fail to do so (so help me out!).

-  Eubanks, Virginia. _Automating Inequality: How High-Tech Tools Profile, Police, and Punish the Poor._, St. Martin's Press, 2018. [**link**](https://us.macmillan.com/books/9781250074317)
-  Floridi, Luciano. _Information: A very short introduction._ Oxford University Press, 2010. [**link**](http://www.veryshortintroductions.com/view/10.1093/actrade/9780199551378.001.0001/actrade-9780199551378)  
-  Gaukroger, Stephen. _Objectivity: A very short introduction._ Oxford University Press, 2012. [**link**](http://www.veryshortintroductions.com/abstract/10.1093/actrade/9780199606696.001.0001/actrade-9780199606696)
- Haraway, Donna J. _Staying with the trouble: Making kin in the Chthulucene._ Duke University Press, 2016. [**link**](https://www.dukeupress.edu/staying-with-the-trouble)
- O'Neil, Cathy. _Weapons of math destruction: How big data increases inequality and threatens democracy._ Broadway Books, 2017. [**link**](https://weaponsofmathdestructionbook.com/)
- Sinnott-Armstrong, Walter. _Think Again: How to Reason and Argue._ Pelican, 2018. [**link**](https://www.penguin.co.uk/books/293942/think-again/9780141983110.html)
- Noble, Safiya Umoja. _Algorithms of oppression: How search engines reinforce racism._ New York University Press, 2018.  [**link**](algorithmsofoppression.com)
- Clemens Apprich, Wendy Hui Kyong Chun, Florian Cramer and Hito Steyerl. _Pattern Discrimination._ University of Minnesota and Meson Press, 2019 [**link**](https://www.upress.umn.edu/book-division/books/pattern-discrimination)

# Glossary entries

Here are some suggestion about what entries you can write about. There's not a mandatory length of the entry, but indicatively I believe that one or two pages should be enough (a part from media content such as videos, pictures, ...). Remember to write the entry for the larger audience (not for me, say, but for one of your colleagues not taking this course). Remember to write it from the point of view of a data scientist or data practitioner (not strictly from the point of view of a user, say, but from the point of view of who takes technical decisions). Rember to write about the entry in the context of data science, and not in general.

## 2019 entries

-  Bias
-  Neutral
-  Raw Data
-  Homophily
-  Infosphere
-  Differential Privacy
-  Privacy as friction
-  Agency
-  Fairness
-  Recommender System
-  Ontic Trust
-  Data Sovereignty
-  Responsibility
-  Digital Poverty Cages
-  Level of Abstraction
-  Weapons of Math Destruction
-  Te Mana Raraunga
-  Interpretability
